﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DemoScene : MonoBehaviour {

	public Slider slider;

	public void Submit()
	{
		GraphOverlay.Instance.LogTimed("ctest", slider.value);
	}

	public void SubmitRandom()
	{
		GraphOverlay.Instance.LogTimed("test", Random.Range(0f, 1f));
	}

}
