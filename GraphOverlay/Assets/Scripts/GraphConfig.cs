﻿using UnityEngine;
using System.Collections;
using System;

public enum GraphType
{
	Valued,
	Counter,
	CounterPerSec,
}

[Serializable]
public class GraphConfig
{
	public string name;

	public Color color = Color.white;
	[Range(0.1f, 4)]
	public float width = 1;
	public float sizeInSecs = 10;

	public GraphType type = GraphType.Valued;

	[Range(0, 60)]
	public float refreshInterval = 1;

	public bool scaledTime = true;

	/// <summary>
	/// The size of the history in records, not seconds.
	/// </summary>
	public int historySize = 50;
}

