﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;

public class GraphOverlay : MonoBehaviour 
{
	private static GraphOverlay _instance;
	public static GraphOverlay Instance 
	{
		get 
		{
			if (_instance == null) 
			{
				var template = Resources.Load<GameObject>("graphOverlay");
				var go = GameObject.Instantiate(template);
				_instance = go.GetComponent<GraphOverlay>();
			}
			return _instance;
		} 
	}

	void Awake()
	{
		_instance = this;
		_template.gameObject.SetActive(false);
		_labelTemplate.gameObject.SetActive(false);

		_labelsContainer = (RectTransform)_topLabels.parent;
	}

	public Color[] colors = new Color[] {
		Color.green,
		Color.blue,
		Color.red,
		Color.yellow,
		Color.magenta,
		Color.cyan,
	};

	[SerializeField]
	protected Graph _template;
	[SerializeField]
	protected Text _labelTemplate;

	[SerializeField]
	protected RectTransform _topLabels;
	[SerializeField]
	protected RectTransform _middleLabels;

	[SerializeField]
	protected GraphConfig defaultGraphConfig = new GraphConfig();
	[SerializeField]
	protected GraphConfig[] graphConfigs;

	private Dictionary<string, Graph> _graphs = new Dictionary<string, Graph>();
	private RectTransform _labelsContainer;

	public void LogTimed(string graphName, float value)
	{
		Graph graph;
		if (!_graphs.TryGetValue(graphName, out graph)) {
			graph = GameObject.Instantiate<Graph>(_template);
			graph.gameObject.SetActive(true);
			graph.gameObject.transform.SetParent(_template.transform.parent, false);

			// Move labels to front
			_labelsContainer.SetAsLastSibling();

			_graphs.Add(graphName, graph);

			var config = graphConfigs.Where(cfg => cfg.name == graphName).DefaultIfEmpty(defaultGraphConfig).First();
			graph.Setup(config, CreateLabels(graphName));
		}

		graph.Add(value);
	}

	public void Count(string graphName, float count = 1)
	{
		LogTimed(graphName, count);
	}

	public void Inc(string graphName, float count = 1)
	{
		LogTimed(graphName, count);
	}

	public void Dec(string graphName, float count = 1)
	{
		LogTimed(graphName, -count);
	}

	private Graph.GraphLabels CreateLabels(string graphName)
	{
		var topLabel = Instantiate<Text>(_labelTemplate);
		topLabel.name = graphName + "-topValue";
		topLabel.transform.SetParent(_topLabels, false);
		topLabel.gameObject.SetActive(true);

		var middleLabel = Instantiate<Text>(_labelTemplate);
		middleLabel.name = graphName + "-middleValue";
		middleLabel.transform.SetParent(_middleLabels, false);
		middleLabel.gameObject.SetActive(true);

		return new Graph.GraphLabels { topText = topLabel, middleText = middleLabel };
	}

}
