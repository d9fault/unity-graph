using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Graph : MonoBehaviour
{
	[System.Serializable]
	public class GraphLabels
	{
		public Text topText;
		public Text middleText;
	}

	public bool autoResize = true;
	private GraphConfig _config;

	[SerializeField]
	protected UILineDrawer _drawer;
	protected GraphLabels _labels;

	protected Deque<Vector2> _points;
	private bool _changed;

	public void Setup(GraphConfig config, GraphLabels labels)
	{
		_config = config;
		_labels = labels;

		_labels.topText.color = _config.color;
		_labels.middleText.color = _config.color;
		_drawer.color = _config.color;
		_drawer.lineWidth = _config.width;

		_points = new Deque<Vector2>(config.historySize, false);

		if (_config.type == GraphType.CounterPerSec || _config.type == GraphType.Counter) {
			StartCoroutine(Counting());
		}

		StartCoroutine(Refreshing());
	}

	private float GetTime() { return _config.scaledTime ? Time.time : Time.unscaledTime; }

	private float _count;

	public void Add (float value)
	{
		if (_config.type == GraphType.Valued) {
			Vector2 point = new Vector2(GetTime(), value);
			_points.AddBack(point);
			_changed = true;
		} else {
			_count += value;
		}
	}

	private IEnumerator Counting()
	{
		while (true) {
			yield return new WaitForSeconds(1);

			_points.AddBack(new Vector2(GetTime(), _count));
			_changed = true;

			if (_config.type == GraphType.CounterPerSec) {
				_count = 0;
			}
		}
	}

	private IEnumerator Refreshing()
	{
		yield return null;
		
		while (true) {
			if (_config.refreshInterval > 0) {
				yield return new WaitForSeconds(_config.refreshInterval);
			} else {
				yield return null;
			}

			UpdateGraph();
		}
	}

	private void UpdateGraph()
	{
		if (!_changed) {
			return;
		}
		_changed = false;

		_drawer.Clear();

		if (_points.Count > 1) 
		{
			int i = _points.Count;
			float last = GetTime();
			float maxValue = float.MinValue;
			float minValue = float.MaxValue;
			float secs = _config.sizeInSecs;

			float time = 0;
			do {
				--i;
				Vector2 point = _points[i];

				if (maxValue < point.y) {
					maxValue = point.y;
				}

				if (minValue > point.y) {
					minValue = point.y;
				}

				time = last - point.x;
			} while (time <= secs && i > 0);

			maxValue = Mathf.Max(maxValue, 1);
			minValue = Mathf.Min(minValue, 0);

			_labels.topText.text = maxValue.ToString("N");
			_labels.middleText.text = ((maxValue - minValue) / 2f).ToString("N");

			Vector2 offset = new Vector2(Mathf.Max(_points[i].x, last - secs), minValue);

			// Fill drawer
			for (; i < _points.Count; i++) 
			{
				_drawer.AddPoint(_points[i]);
			}

			// Calculate scale
			_drawer.Apply(offset, new Vector2(secs, maxValue));
		}
		else 
		{
			_drawer.Apply(Vector2.zero, Vector2.one);
		}
	}

}

