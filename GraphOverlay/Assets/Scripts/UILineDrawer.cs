using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class UILineDrawer : Graphic
{
	public float lineWidth = 2f;
	[SerializeField]
	private List<Vector2> _points;

	private readonly Vector3[] offsets = new Vector3[] {
		new Vector3(-1, 0, 0),
		new Vector3(1, 0, 0),
	};
	private UIVertex[] _quad = new UIVertex[4];

	private Vector2 _offset;
	private Vector2 _dimentions;
	private Vector2 _scale;

	public void Clear()
	{
		if (_points == null) {
			_points = new List<Vector2>();
		}
		_points.Clear();
	}

	public void AddPoint (Vector2 point)
	{
		if (_points == null) {
			_points = new List<Vector2>();
		}
		_points.Add(point);
	}

	public void Apply(Vector2 offset, Vector2 dimensions)
	{
		_offset = offset;
		_dimentions = dimensions;
		SetVerticesDirty();
	}

	protected override void OnPopulateMesh (VertexHelper vh)
	{
		Rect pixelAdjustedRect = this.GetPixelAdjustedRect ();

		vh.Clear();

		if (_points == null || _points.Count < 2) {
			return;
		}

		Vector2 size = rectTransform.rect.size;
		_scale = new Vector2(size.x / _dimentions.x, size.y / _dimentions.y);

		for (int i = 0; i < _points.Count - 1; i++) {
			Vector3 firstPoint = _points[i];
			Vector3 secondPoint = _points[i + 1];

			Vector3 dir = (secondPoint - firstPoint).normalized;
			var rotation = Quaternion.FromToRotation(new Vector3(0, 1, 0), dir);

			_quad [0] = GetVertex (firstPoint, rotation, 0);
			_quad [1] = GetVertex (firstPoint, rotation, 1);
			_quad [2] = GetVertex (secondPoint, rotation, 1);
			_quad [3] = GetVertex (secondPoint, rotation, 0);

			vh.AddUIVertexQuad(_quad);
		}
	}

	UIVertex GetVertex (Vector3 point, Quaternion rotation, int offsetIndex)
	{
		point -= new Vector3(_offset.x, _offset.y, 0);

		Vector3 pos = point;
		pos.Scale(_scale);
		pos += rotation * (offsets [offsetIndex] * lineWidth);

		return new UIVertex {
			color = color,
			position = pos,
		};
	}
}

