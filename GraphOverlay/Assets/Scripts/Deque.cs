﻿using DequeUtility;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace System.Collections.Generic
{
    /// <summary>
    /// A genetic Deque class. It can be thought of as
    /// a double-ended queue, hence Deque. This allows for
    /// an O(1) AddFront, AddBack, RemoveFront, RemoveBack.
    /// The Deque also has O(1) indexed lookup, as it is backed
    /// by a circular array.
    /// </summary>
    /// <typeparam name="T">
    /// The type of objects to store in the deque.
    /// </typeparam>
	public class Deque<T> : IEnumerable<T>
    {

        /// <summary>
        /// The default capacity of the deque.
        /// </summary>
        private const int defaultCapacity = 16;

        /// <summary>
        /// The first element offset from the beginning of the data array.
        /// </summary>
        private int startOffset;

        /// <summary>
        /// The circular array holding the items.
        /// </summary>
        private T[] buffer;

        /// <summary>
        /// Creates a new instance of the Deque class with
        /// the default capacity.
        /// </summary>
        public Deque() : this(defaultCapacity, true) { }

        /// <summary>
        /// Creates a new instance of the Deque class with
        /// the specified capacity.
        /// </summary>
        /// <param name="capacity">The initial capacity of the Deque.</param>
		public Deque(int capacity, bool canGrow)
        {
            if (capacity < 0)
            {
                throw new ArgumentOutOfRangeException(
                    "capacity", "capacity is less than 0.");
            }

            this.Capacity = capacity;
			this.CanGrow = canGrow;
        }

        private int capacityClosestPowerOfTwoMinusOne;

        /// <summary>
        /// Gets or sets the total number of elements
        /// the internal array can hold without resizing.
        /// </summary>
        public int Capacity
        {
            get
            {
                return buffer.Length;
            }

            set
            {
                if (value < 0)
                {
                    throw new ArgumentOutOfRangeException(
                        "value",
                        "Capacity is less than 0.");
                }
                else if (value < this.Count)
                {
                    throw new InvalidOperationException(
                        "Capacity cannot be set to a value less than Count");
                }
                else if (null != buffer && value == buffer.Length)
                {
                    return;
                }

                // Create a new array and copy the old values.
                int powOfTwo = Utility.ClosestPowerOfTwoGreaterThan(value);

                value = powOfTwo;

                T[] newBuffer = new T[value];
                this.CopyTo(newBuffer, 0);

                // Set up to use the new buffer.
                buffer = newBuffer;
                startOffset = 0;
                this.capacityClosestPowerOfTwoMinusOne = powOfTwo - 1;
            }
        }

		public bool CanGrow { get; private set; }

        /// <summary>
        /// Gets whether or not the Deque is filled to capacity.
        /// </summary>
        public bool IsFull
        {
            get { return this.Count == this.Capacity; }
        }

        /// <summary>
        /// Gets whether or not the Deque is empty.
        /// </summary>
        public bool IsEmpty
        {
            get { return 0 == this.Count; }
        }

		private void ensureCapacityFor(int numElements, bool front)
        {
            if (this.Count + numElements > this.Capacity)
            {
				if (CanGrow) 
				{
					this.Capacity = this.Count + numElements;
				} else if (front) {
					RemoveBack();
				} else {
					RemoveFront();
				}
            }
        }

        private int toBufferIndex(int index)
        {
            int bufferIndex;

            bufferIndex = (index + this.startOffset)
                & this.capacityClosestPowerOfTwoMinusOne;

            return bufferIndex;
        }

        private void checkIndexOutOfRange(int index)
        {
            if (index >= this.Count)
            {
                throw new IndexOutOfRangeException(
                    "The supplied index is greater than the Count");
            }
        }

        private static void checkArgumentsOutOfRange(
            int length,
            int offset,
            int count)
        {
            if (offset < 0)
            {
                throw new ArgumentOutOfRangeException(
                    "offset", "Invalid offset " + offset);
            }

            if (count < 0)
            {
                throw new ArgumentOutOfRangeException(
                    "count", "Invalid count " + count);
            }

            if (length - offset < count)
            {
                throw new ArgumentException(
                    String.Format(
                    "Invalid offset ({0}) or count + ({1}) "
                    + "for source length {2}",
                    offset, count, length));
            }
        }

        private int shiftStartOffset(int value)
        {
            this.startOffset = toBufferIndex(value);

            return this.startOffset;
        }

        private int preShiftStartOffset(int value)
        {
            int offset = this.startOffset;
            this.shiftStartOffset(value);
            return offset;
        }

        private int postShiftStartOffset(int value)
        {
            return shiftStartOffset(value);
        }

        #region IEnumerable

        /// <summary>
        /// Returns an enumerator that iterates through the Deque.
        /// </summary>
        /// <returns>
        /// An iterator that can be used to iterate through the Deque.
        /// </returns>
        public IEnumerator<T> GetEnumerator()
        {

            // The below is done for performance reasons.
            // Rather than doing bounds checking and modulo arithmetic
            // that would go along with calls to Get(index), we can skip
            // all of that by referencing the underlying array.

            if (this.startOffset + this.Count > this.Capacity)
            {
                for (int i = this.startOffset; i < this.Capacity; i++)
                {
                    yield return buffer[i];
                }

                int endIndex = toBufferIndex(this.Count);
                for (int i = 0; i < endIndex; i++)
                {
                    yield return buffer[i];
                }
            }
            else
            {
                int endIndex = this.startOffset + this.Count;
                for (int i = this.startOffset; i < endIndex; i++)
                {
                    yield return buffer[i];
                }
            }
        }

        /// <summary>
        /// Returns an enumerator that iterates through the Deque.
        /// </summary>
        /// <returns>
        /// An iterator that can be used to iterate through the Deque.
        /// </returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
        #endregion

        /// <summary>
        /// Gets the number of elements contained in the Deque.
        /// </summary>
        public int Count
        {
            get;
            private set;
        }

        private void incrementCount(int value)
        {
            this.Count = this.Count + value;
        }

        private void decrementCount(int value)
        {
            this.Count = Math.Max(this.Count - value, 0);
        }

        private void ClearBuffer(int logicalIndex, int length)
        {
            int offset = toBufferIndex(logicalIndex);
            if (offset + length > this.Capacity)
            {
                int len = this.Capacity - offset;
                Array.Clear(this.buffer, offset, len);

                len = toBufferIndex(logicalIndex + length);
                Array.Clear(this.buffer, 0, len);
            }
            else
            {
                Array.Clear(this.buffer, offset, length);
            }
        }

        /// <summary>
        /// Removes all items from the Deque.
        /// </summary>
        public void Clear()
        {
            if (this.Count > 0)
            {
                ClearBuffer(0, this.Count);
            }
            this.Count = 0;
            this.startOffset = 0;
        }

        /// <summary>
        /// Determines whether the Deque contains a specific value.
        /// </summary>
        /// <param name="item">The object to locate in the Deque.</param>
        /// <returns>
        /// true if item is found in the Deque; otherwise, false.
        /// </returns>
        public bool Contains(T item)
        {
            return this.IndexOf(item) != -1;
        }
        
        /// <summary>
        ///     Copies the elements of the Deque to a System.Array,
        ///     starting at a particular System.Array index.
        /// </summary>
        /// <param name="array">
        ///     The one-dimensional System.Array that is the destination of
        ///     the elements copied from the Deque. The System.Array must
        ///     have zero-based indexing.
        /// </param>
        /// <param name="arrayIndex">
        ///     The zero-based index in array at which copying begins.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        ///     array is null.
        /// </exception>
        /// <exception cref="System.ArgumentOutOfRangeException">
        ///     arrayIndex is less than 0.
        /// </exception>
        /// <exception cref="System.ArgumentException">
        ///     The number of elements in the source Deque is greater than
        ///     the available space from arrayIndex to the end of the
        ///     destination array.
        /// </exception>
        public void CopyTo(T[] array, int arrayIndex)
        {
            if (null == array)
            {
                throw new ArgumentNullException("array", "Array is null");
            }

            // Nothing to copy
            if (null == this.buffer)
            {
                return;
            }

            checkArgumentsOutOfRange(array.Length, arrayIndex, this.Count);

            if (0 != this.startOffset
                && this.startOffset + this.Count >= this.Capacity)
            {
                int lengthFromStart = this.Capacity - this.startOffset;
                int lengthFromEnd = this.Count - lengthFromStart;

                Array.Copy(
                    buffer, this.startOffset, array, 0, lengthFromStart);

                Array.Copy(
                    buffer, 0, array, lengthFromStart, lengthFromEnd);
            }
            else
            {
                Array.Copy(
                    buffer, this.startOffset, array, 0, Count);
            }
        }

        /// <summary>
        /// Gets or sets the element at the specified index.
        /// </summary>
        /// <param name="index">
        ///     The zero-based index of the element to get or set.
        /// </param>
        /// <returns>The element at the specified index</returns>
        /// <exception cref="System.ArgumentOutOfRangeException">
        ///     <paramref name="index"/> is not a valid index in this deque
        /// </exception>
        public T this[int index]
        {
            get
            {
                return this.Get(index);
            }

            set
            {
                this.Set(index, value);
            }
        }

        /// <summary>
        /// Determines the index of a specific item in the deque.
        /// </summary>
        /// <param name="item">The object to locate in the deque.</param>
        /// <returns>
        /// The index of the item if found in the deque; otherwise, -1.
        /// </returns>
        public int IndexOf(T item)
        {
            int index = 0;
            foreach (var myItem in this)
            {
                if (myItem.Equals(item))
                {
                    break;
                }
                ++index;
            }

            if (index == this.Count)
            {
                index = -1;
            }

            return index;
        }

        /// <summary>
        /// Adds the provided item to the front of the Deque.
        /// </summary>
        /// <param name="item">The item to add.</param>
        public void AddFront(T item)
        {
            ensureCapacityFor(1, true);
            buffer[postShiftStartOffset(-1)] = item;
            incrementCount(1);
        }

        /// <summary>
        /// Adds the provided item to the back of the Deque.
        /// </summary>
        /// <param name="item">The item to add.</param>
        public void AddBack(T item)
        {
			ensureCapacityFor(1, false);
            buffer[toBufferIndex(this.Count)] = item;
            incrementCount(1);
        }

        /// <summary>
        /// Removes an item from the front of the Deque and returns it.
        /// </summary>
        /// <returns>The item at the front of the Deque.</returns>
        public T RemoveFront()
        {
            if (this.IsEmpty)
            {
                throw new InvalidOperationException("The Deque is empty");
            }

            T result = buffer[this.startOffset];
            buffer[preShiftStartOffset(1)] = default(T);
            decrementCount(1);
            return result;
        }

        /// <summary>
        /// Removes an item from the back of the Deque and returns it.
        /// </summary>
        /// <returns>The item in the back of the Deque.</returns>
        public T RemoveBack()
        {
            if (this.IsEmpty)
            {
                throw new InvalidOperationException("The Deque is empty");
            }

            decrementCount(1);
            int endIndex = toBufferIndex(this.Count);
            T result = buffer[endIndex];
            buffer[endIndex] = default(T);
            
            return result;
        }

        /// <summary>
        /// Gets the value at the specified index of the Deque
        /// </summary>
        /// <param name="index">The index of the Deque.</param>
        /// <returns></returns>
        public T Get(int index)
        {
            checkIndexOutOfRange(index);
            return buffer[toBufferIndex(index)];
        }

        /// <summary>
        /// Sets the value at the specified index of the
        /// Deque to the given item.
        /// </summary>
        /// <param name="index">The index of the deque to set the item.</param>
        /// <param name="item">The item to set at the specified index.</param>
        public void Set(int index, T item)
        {
            checkIndexOutOfRange(index);
            buffer[toBufferIndex(index)] = item;
        }

    }
}
