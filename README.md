# Graph types

### Value
Track value's dynamics of some metric.
For example. You have number of objects in scene and want to visualize it.
```C#
GraphOverlay.Instance.LogTimed("objectsCount", count);
```
* For this kind of grapgh `count` parameter **must** be provided.
* For each `LogTimed` call will be created node in graph. *This not good idea to calls
it too often.*
* Averaging of values during a second is not implemented yet

### Counter
Does counting of all passed values and show one node in graph per second.
*I don't know is any usage for this shit.*

### Counter per second
It is like **Counter** type but reset counter after node has added to graph.
Very usefull for visualize some actions per second.
For example - requests per second to server.
```C#
GraphOverlay.Instance.Inc("serverRequests");    // Every request
```
